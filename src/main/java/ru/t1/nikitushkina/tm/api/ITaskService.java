package ru.t1.nikitushkina.tm.api;

import ru.t1.nikitushkina.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task create(String name, String description);

    void clear();

    Task add(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task project);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
