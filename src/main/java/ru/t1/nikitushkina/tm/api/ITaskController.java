package ru.t1.nikitushkina.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
