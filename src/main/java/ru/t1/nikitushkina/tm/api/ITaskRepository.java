package ru.t1.nikitushkina.tm.api;

import ru.t1.nikitushkina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void clear();

    Task add(Task task);

    Task create(String name, String description);

    Task create(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
